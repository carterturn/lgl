#pragma once

#include <GL/gl.h>

#include <string>

using std::string;

class lgl_text {
public:
	lgl_text(string font_bitmap);

	int initialize();

	// Deprecated
	void draw_text(string text, float x_start, float y_start, float total_length, float R, float G, float B);

	// height and length are maximums
	void draw_text(string text, float x_start, float y_start, float height, float length, float R, float G, float B);

private:
	string m_font_bitmap_filename;
	GLuint m_font_texture;
};
