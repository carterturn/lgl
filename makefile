ifndef WINDOWS
CPP=g++
LFLAGS=-lGL -lglfw
CFLAGS=
LIBRARY=liblgl.so
else
CPP=x86_64-w64-mingw32-c++
LFLAGS=-Lwindows_libraries/glfw-3.2.1.bin.WIN64/lib-mingw-w64/ -lglfw3 -L~/.wine/drive_c/windows/syswow64/ -lopengl32 -lgdi32
CFLAGS=-Iwindows_libraries/glfw-3.2.1.bin.WIN64/include/ -DWINDOWS=1
LIBRARY=lgl.dll
endif

TEST_SRC = test.cpp

# The core includes used by almost all files
CORE_INC = lgl_object.h lgl_window.h lgl_primatives.h lgl_text.h
CORE_SRC = lgl_object.cpp lgl_window.cpp lgl_primatives.cpp lgl_text.cpp

# Common objects
INC = lgl_button.h lgl_indicator.h lgl_elbow.h lgl_edit_indicator.h lgl_text_display.h
SRC = lgl_button.cpp lgl_indicator.cpp lgl_elbow.cpp lgl_edit_indicator.cpp lgl_text_display.cpp

EXE = test

build: $(SRC) $(CORE_SRC) $(INC) $(CORE_INC)
	$(CPP) -c -fPIC $(CORE_SRC) $(CFLAGS)
	$(CPP) -c -fPIC $(SRC) $(CFLAGS)
	$(CPP) -shared -o $(LIBRARY) *.o $(LFLAGS)
test: $(TEST_SRC) build
	$(CPP) -o $(EXE) $(TEST_SRC) *.o $(CFLAGS) $(LFLAGS)
install:
	[ -d /usr/include/lgl ] || mkdir /usr/include/lgl
	cp -f *.h /usr/include/lgl/
	cp -f liblgl.so /usr/lib/
	chmod a+r -R /usr/include/lgl
	chmod a+r /usr/lib/liblgl.so
uninstall:
	[ -d /usr/include/lgl ] && rm -f /usr/include/lgl/*.h
	[ -d /usr/include/lgl/* ] && rmdir /usr/include/lgl
	rm -f /usr/lib/liblgl.so
clean:
	for file in $$(ls *.o); do rm $$file; done
	for file in $$(ls *.so); do rm $$file; done
	for file in $$(ls *.gch); do rm $$file; done
	if [ -e $(EXE) ]; then rm $(EXE); fi
rebuild: clean build
