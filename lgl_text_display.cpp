/*
  Copyright 2018 Carter Turnbaugh

  This file is part of LGL.

  LGL is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  LGL is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with LGL.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lgl_primatives.h"
#include "lgl_text_display.h"

#include <iostream>

using std::cout;

lgl_text_display::lgl_text_display(int grid_x, int grid_y, int height, int width,
								   lgl_color color, lgl_text * text_engine)
	: lgl_object(text_engine), m_color(color), m_data_start(0), m_height(height), m_width(width){
	m_grid_x_min = grid_x;
	m_grid_y_min = grid_y;
	m_grid_x_max = grid_x+width;
	m_grid_y_max = grid_y+height;

	m_scale_draw = false;
}

void lgl_text_display::set_text_data(vector<string> data){
	m_data = data;
}

void lgl_text_display::draw(){
	lgl_color text_color = m_color;

	for(int i = m_data_start; i < 2*m_height+m_data_start; i+=2){
		if(i >= m_data.size()){
			break;
		}
		if(m_capture_input && m_hover_y > (i-m_data_start)*0.5 && m_hover_y < (i+1-m_data_start)*0.5){
			text_color = {0.0f, 0.0f, 0.0f};
			lgl_set_color(m_color.R, m_color.G, m_color.B);
			lgl_draw_rectangle(0.0f, (i-m_data_start)*0.5, m_width, 0.5);
		}
		else{
			text_color = m_color;
		}
		m_text_engine->draw_text(m_data[i], 0.0625, 0.0625 + (i-m_data_start)*0.5,
								 0.5-0.03125, m_width - 0.125, text_color.R, text_color.G, text_color.B);
		if(i+1 >= m_data.size()){
			break;
		}
		if(m_capture_input && m_hover_y > (i+1-m_data_start)*0.5 && m_hover_y < (i+2-m_data_start)*0.5){
			text_color = {0.0f, 0.0f, 0.0f};
			lgl_set_color(m_color.R, m_color.G, m_color.B);
			lgl_draw_rectangle(0.0f, (i+1-m_data_start)*0.5, m_width, 0.5);
		}
		else{
			text_color = m_color;
		}
		m_text_engine->draw_text(m_data[i+1], 0.0625, 0.03125 + (i+1-m_data_start)*0.5,
								 0.5-0.03125, m_width - 0.125, text_color.R, text_color.G, text_color.B);
	}
}

void lgl_text_display::on_hover_start(float x, float y){
	m_capture_input = true;

	m_hover_y = y;
}

void lgl_text_display::on_hover_end(){
	m_capture_input = false;
}

void lgl_text_display::on_left_mouse_press(float x, float y){
	int index = (int) (y * 2.0f) + m_data_start;

	on_left_mouse_press(m_data[index]);
}

void lgl_text_display::on_left_mouse_press(string item){}

void lgl_text_display::on_scroll(double x_offset, double y_offset){
	m_data_start += y_offset;
	if(m_data_start < 0){
		m_data_start = 0;
	}
	else if(m_data_start > m_data.size() - 2*m_height){
		m_data_start = m_data.size() - 2*m_height;
	}
}

