/*
  Copyright 2018 Carter Turnbaugh

  This file is part of LGL.

  LGL is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  LGL is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with LGL.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lgl_window.h"

#include <algorithm>

using std::find_if;

lgl_window::lgl_window(string title, int grid_x, int grid_y) : m_title(title), m_grid_x(grid_x), m_grid_y(grid_y) {
	m_objects = list<lgl_object *>();
	m_keep_running = true;

	if(m_grid_x < 1){
		m_grid_x = 1;
	}
	if(m_grid_y < 1){
		m_grid_y = 1;
	}
}

int lgl_window::initialize(){
	glfwInit();

	m_glfw_window = glfwCreateWindow(GRID_WIDTH_FULL * m_grid_x, GRID_HEIGHT_FULL * m_grid_y,
									 m_title.c_str(), NULL, NULL);

	if(!m_glfw_window){
		return -1;
	}

	glfwSetWindowUserPointer(m_glfw_window, (void *) this);

	glfwMakeContextCurrent(m_glfw_window);

	glfwSetMouseButtonCallback(m_glfw_window, lgl_window::on_click_bootstrap);
	glfwSetCursorPosCallback(m_glfw_window, lgl_window::cursor_position_bootstrap);
	glfwSetCursorEnterCallback(m_glfw_window, lgl_window::cursor_enter_bootstrap);
	glfwSetKeyCallback(m_glfw_window, lgl_window::keyboard_bootstrap);
	glfwSetCharCallback(m_glfw_window, lgl_window::text_input_bootstrap);
	glfwSetScrollCallback(m_glfw_window, lgl_window::scroll_bootstrap);

	return 0;
}

void lgl_window::cleanup(){
	glfwDestroyWindow(m_glfw_window);
}

void lgl_window::resize(){
	int grid_x = (*max_element(m_objects.begin(), m_objects.end(), [](lgl_object * a, lgl_object * b)
	{return a->get_grid_x_max() < b->get_grid_x_max();}))->get_grid_x_max();
	int grid_y = (*max_element(m_objects.begin(), m_objects.end(), [](lgl_object * a, lgl_object * b)
	{return a->get_grid_y_max() < b->get_grid_y_max();}))->get_grid_y_max();

	resize(grid_x, grid_y);
}

void lgl_window::resize(int grid_x, int grid_y){
	m_grid_x = grid_x;
	m_grid_y = grid_y;

	glfwMakeContextCurrent(m_glfw_window);
	glfwSetWindowSize(m_glfw_window, GRID_WIDTH_FULL * m_grid_x, GRID_HEIGHT_FULL * m_grid_y);
	glViewport(0, 0, GRID_WIDTH_FULL * m_grid_x, GRID_HEIGHT_FULL * m_grid_y);
}

void lgl_window::add_object(lgl_object * new_object){
	m_objects.push_back(new_object);
}

void lgl_window::clear_objects(){
	while(m_objects.size() > 0){
		delete m_objects.front();
		m_objects.pop_front();
	}
}

void lgl_window::update(){
	glfwMakeContextCurrent(m_glfw_window);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, m_grid_x, 0, m_grid_y, -1, 1);

	glClear(GL_COLOR_BUFFER_BIT);

	for(lgl_object * o : m_objects){
		glTranslatef(o->get_grid_x_min(), o->get_grid_y_min(), 0);

		if(o->get_scale_draw()){
			float grid_x_scale = (float) (o->get_grid_x_max() - o->get_grid_x_min());
			float grid_y_scale = (float) (o->get_grid_y_max() - o->get_grid_y_min());

			glScalef(grid_x_scale, grid_y_scale, 1.0f);
		}

		o->draw();

		if(o->get_scale_draw()){
			float grid_x_scale = 1.0f / (float) (o->get_grid_x_max() - o->get_grid_x_min());
			float grid_y_scale = 1.0f / (float) (o->get_grid_y_max() - o->get_grid_y_min());

			glScalef(grid_x_scale, grid_y_scale, 1.0f);
		}

		glTranslatef(-o->get_grid_x_min(), -o->get_grid_y_min(), 0);
	}

	glfwSwapBuffers(m_glfw_window);
	glfwPollEvents();

	if(glfwWindowShouldClose(m_glfw_window)){
		m_keep_running = false;
	}
}

bool lgl_window::keep_running(){
	return m_keep_running;
}

void lgl_window::on_click(float x, float y, int button, int action, int modifiers){

	x = x / (float) GRID_WIDTH_FULL;
	y = (float) m_grid_y - y / (float) GRID_HEIGHT_FULL;

	for(lgl_object * o : m_objects){
		if(x > o->get_grid_x_max() || x < o->get_grid_x_min()
		   || y > o->get_grid_y_max() || y < o->get_grid_y_min()){
			continue;
		}
		
		float o_x = x - o->get_grid_x_min();
		float o_y = y - o->get_grid_y_min();

		if(o->get_scale_draw()){
			float grid_x_scale = (float) (o->get_grid_x_max() - o->get_grid_x_min());
			float grid_y_scale = grid_x_scale * (float) GRID_WIDTH_FULL / (float) GRID_HEIGHT_FULL;

			o_x = o_x / grid_x_scale;
			o_y = o_y / grid_y_scale;
		}

		if(button == GLFW_MOUSE_BUTTON_RIGHT){
			if(action == GLFW_PRESS){
				o->on_right_mouse_press(o_x, o_y);
			}
			else if(action == GLFW_RELEASE){
				o->on_right_mouse_release(o_x, o_y);
			}
		}
		else if(button == GLFW_MOUSE_BUTTON_LEFT){
			if(action == GLFW_PRESS){
				o->on_left_mouse_press(o_x, o_y);
			}
			else if(action == GLFW_RELEASE){
				o->on_left_mouse_release(o_x, o_y);
			}
		}
	}
}

void lgl_window::cursor_position(float x, float y){
	x = x / (float) GRID_WIDTH_FULL;
	y = (float) m_grid_y - y / (float) GRID_HEIGHT_FULL;

	for(lgl_object * o : m_objects){
		if(x < o->get_grid_x_max() && x > o->get_grid_x_min()
		   && y < o->get_grid_y_max() && y > o->get_grid_y_min()){
			float o_x = x - o->get_grid_x_min();
			float o_y = y - o->get_grid_y_min();

			if(o->get_scale_draw()){
				float grid_x_scale = (float) (o->get_grid_x_max() - o->get_grid_x_min());
				float grid_y_scale = grid_x_scale * (float) GRID_WIDTH_FULL / (float) GRID_HEIGHT_FULL;

				o_x = o_x / grid_x_scale;
				o_y = o_y / grid_y_scale;
			}

			o->on_hover_start(o_x, o_y);
		}
		else{
			o->on_hover_end();
		}
	}
}

void lgl_window::cursor_enter(int entered){
	if(entered == GLFW_FALSE){
		for(lgl_object * o : m_objects){
			o->on_hover_end();
		}
	}
}

void lgl_window::keyboard(int key, int scancode, int action, int mods){
	for(lgl_object * o : m_objects){
		if(o->get_capture_input()){
			o->on_keyboard(key, scancode, action, mods);
		}
	}
}

void lgl_window::text_input(unsigned int key){
	for(lgl_object * o : m_objects){
		if(o->get_capture_input()){
			o->on_text_input(key);
		}
	}
}

void lgl_window::scroll(double x_offset, double y_offset){
	for(lgl_object * o : m_objects){
		if(o->get_capture_input()){
			o->on_scroll(x_offset, y_offset);
		}
	}
}

void lgl_window::on_click_bootstrap(GLFWwindow * glfw_window, int button, int action, int modifiers){
	double mouse_x, mouse_y;
	glfwGetCursorPos(glfw_window, &mouse_x, &mouse_y);
	((lgl_window *) glfwGetWindowUserPointer(glfw_window))->on_click(mouse_x, mouse_y, button, action, modifiers);
}

void lgl_window::cursor_position_bootstrap(GLFWwindow * glfw_window, double x, double y){
	((lgl_window *) glfwGetWindowUserPointer(glfw_window))->cursor_position(x, y);
}

void lgl_window::cursor_enter_bootstrap(GLFWwindow * glfw_window, int entered){
	((lgl_window *) glfwGetWindowUserPointer(glfw_window))->cursor_enter(entered);
}

void lgl_window::keyboard_bootstrap(GLFWwindow * glfw_window, int key, int scancode, int action, int mods){
	((lgl_window *) glfwGetWindowUserPointer(glfw_window))->keyboard(key, scancode, action, mods);
}

void lgl_window::text_input_bootstrap(GLFWwindow * glfw_window, unsigned int key){
	((lgl_window *) glfwGetWindowUserPointer(glfw_window))->text_input(key);
}

void lgl_window::scroll_bootstrap(GLFWwindow * glfw_window, double x_offset, double y_offset){
	((lgl_window *) glfwGetWindowUserPointer(glfw_window))->scroll(x_offset, y_offset);
}
