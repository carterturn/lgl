#pragma once

#include "lgl_button.h"
#include "lgl_elbow.h"

class lgl_button_color_dim : public lgl_button {
public:
	lgl_button_color_dim(int grid_x, int grid_y, int height, string text, lgl_color color, lgl_text * text_engine)
		: lgl_button(grid_x, grid_y, height, text, color, text_engine), m_base_color(color) {}

	void on_hover_start(){
		set_color(m_base_color.R * 0.75f, m_base_color.G * 0.75f, m_base_color.B * 0.75f);
	}

	void on_hover_end(){
		set_color(m_base_color);
	}

protected:
	lgl_color m_base_color;
};

class lgl_elbow_color_dim : public lgl_elbow {
public:
	lgl_elbow_color_dim(int grid_x, int grid_y, int height, int length, int orientation,
						string text, lgl_color color, lgl_text * text_engine)
		: lgl_elbow(grid_x, grid_y, height, length, orientation, text, color, text_engine), m_base_color(color) {}

	void on_hover_start(){
		set_color(m_base_color.R * 0.75f, m_base_color.G * 0.75f, m_base_color.B * 0.75f);
	}

	void on_hover_end(){
		set_color(m_base_color);
	}

protected:
	lgl_color m_base_color;
};
