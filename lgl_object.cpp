/*
  Copyright 2018 Carter Turnbaugh

  This file is part of LGL.

  LGL is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  LGL is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with LGL.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "lgl_object.h"

lgl_object::lgl_object(lgl_text * text_engine) : m_text_engine(text_engine), m_capture_input(false) {}

float lgl_object::get_grid_x_min(){
	return m_grid_x_min;
}


float lgl_object::get_grid_x_max(){
	return m_grid_x_max;
}

float lgl_object::get_grid_y_min(){
	return m_grid_y_min;
}

float lgl_object::get_grid_y_max(){
	return m_grid_y_max;
}

bool lgl_object::get_scale_draw(){
	return m_scale_draw;
}

bool lgl_object::get_capture_input(){
	return m_capture_input;
}

void lgl_object::draw(){}

void lgl_object::on_hover_start(float x, float y){}
void lgl_object::on_hover_end(){}
void lgl_object::on_left_mouse_press(float x, float y){}
void lgl_object::on_left_mouse_release(float x, float y){}
void lgl_object::on_right_mouse_press(float x, float y){}
void lgl_object::on_right_mouse_release(float x, float y){}

void lgl_object::on_keyboard(int key, int scancode, int action, int mods){}
void lgl_object::on_text_input(unsigned int key){}

void lgl_object::on_scroll(double x_offset, double y_offset){}
