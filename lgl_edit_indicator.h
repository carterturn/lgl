#pragma once

#include "lgl_indicator.h"

class lgl_edit_indicator : public lgl_indicator {
public:
	lgl_edit_indicator(int grid_x, int grid_y, int width, lgl_color color, lgl_text * text_engine);

	void on_left_mouse_press();

	void on_keyboard(int key, int scancode, int action, int mods);
	void on_text_input(unsigned int key);

	virtual void on_edit_complete();
	virtual void on_edit_cancel();
};
