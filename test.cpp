#include <chrono>
#include <iostream>
#include <thread>

#include "lgl_button.h"
#include "lgl_elbow.h"
#include "lgl_color_dim.h"
#include "lgl_indicator.h"
#include "lgl_edit_indicator.h"
#include "lgl_text.h"
#include "lgl_text_display.h"
#include "lgl_window.h"

using namespace std;
using namespace std::chrono;
using std::this_thread::sleep_for;

class button_1 : public lgl_button_color_dim {
public:
	button_1(int grid_x, int grid_y, int height, string text, lgl_color color, lgl_text * text_engine)
		: lgl_button_color_dim(grid_x, grid_y, height, text, color, text_engine) {
		m_on = false;
	}

	void on_left_mouse_press(){
		m_on = !m_on;
	}

	bool is_on(){
		return m_on;
	}

protected:
	bool m_on;
};

int main(int argc, char * argv[]){
	lgl_window test_window("Test Window");
	if(test_window.initialize() != 0){
		cout << "AAAAHHH!\n";
		return -1;
	}

	lgl_text text_engine("monospace.bmp");
	text_engine.initialize();

	button_1 test_button_1(0, 2, 1, "Button 1", {1.0f, 0.0f, 0.0f}, &text_engine);
	button_1 test_button_2(1, 2, 3, "Button 2", {0.5f, 0.5f, 0.0f}, &text_engine);
	button_1 test_button_3(2, 2, 1, "Button 3", {0.0f, 1.0f, 0.0f}, &text_engine);
	button_1 test_button_4(2, 3, 2, "Button 4", {0.0f, 0.0f, 1.0f}, &text_engine);

	lgl_indicator test_indicator_1(0, 3, 1, {1.0f, 0.0f, 0.0f}, &text_engine);
	test_indicator_1.set_text("OFF");
	lgl_indicator test_indicator_2(0, 4, 1, {0.5f, 0.5f, 0.0f}, &text_engine);
	test_indicator_2.set_text("OFF");
	lgl_indicator test_indicator_3(0, 5, 2, {0.0f, 1.0f, 0.0f}, &text_engine);
	test_indicator_3.set_text("OFF");
	lgl_indicator test_indicator_4(2, 5, 1, {0.0f, 0.0f, 1.0f}, &text_engine);
	test_indicator_4.set_text("OFF");

	lgl_elbow test_elbow_1(0, 0, 0, 0, lgl_elbow::ORIENT_BOTTOM_LEFT, "Elbow 1", {1.0f, 0.0f, 0.0f}, &text_engine);
	lgl_elbow test_elbow_2(0, 6, 0, 0, lgl_elbow::ORIENT_TOP_LEFT, "Elbow 2", {1.0f, 0.0f, 0.0f}, &text_engine);
	lgl_elbow test_elbow_3(2, 0, 2, 1, lgl_elbow::ORIENT_BOTTOM_RIGHT, "Elbow 3", {1.0f, 0.0f, 0.0f}, &text_engine);
	lgl_elbow test_elbow_4(2, 4, 2, 1, lgl_elbow::ORIENT_TOP_RIGHT, "Elbow 4", {1.0f, 0.0f, 0.0f}, &text_engine);

	lgl_edit_indicator edit_indicator_1(3, 2, 1, {0.0f, 1.0f, 0.0f}, &text_engine);
	edit_indicator_1.set_text("1000");

	lgl_text_display test_text_display(3, 3, 2, 1, {0.0f, 1.0f, 1.0f}, &text_engine);
	test_text_display.set_text_data({"Apple", "Banana", "Carrot", "Dumpster", "Evil", "Friday"});

	test_window.add_object(&test_button_1);
	test_window.add_object(&test_button_2);
	test_window.add_object(&test_button_3);
	test_window.add_object(&test_button_4);

	test_window.add_object(&test_indicator_1);
	test_window.add_object(&test_indicator_2);
	test_window.add_object(&test_indicator_3);
	test_window.add_object(&test_indicator_4);

	test_window.add_object(&test_elbow_1);
	test_window.add_object(&test_elbow_2);
	test_window.add_object(&test_elbow_3);
	test_window.add_object(&test_elbow_4);

	test_window.add_object(&edit_indicator_1);

	test_window.add_object(&test_text_display);

	test_window.resize();

	while(test_window.keep_running()){

		test_window.update();

		if(test_button_1.is_on()){
			test_indicator_1.set_text("ON");
		}
		else{
			test_indicator_1.set_text("OFF");
		}

		if(test_button_2.is_on()){
			test_indicator_2.set_text("ON");
		}
		else{
			test_indicator_2.set_text("OFF");
		}

		if(test_button_3.is_on()){
			test_indicator_3.set_text("ON");
		}
		else{
			test_indicator_3.set_text("OFF");
		}

		if(test_button_4.is_on()){
			test_indicator_4.set_text("ON");
		}
		else{
			test_indicator_4.set_text("OFF");
		}

		sleep_for(milliseconds(10));
	}

	return 0;
}
