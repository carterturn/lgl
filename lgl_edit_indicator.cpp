#include "lgl_edit_indicator.h"

#include "lgl_window.h"

lgl_edit_indicator::lgl_edit_indicator(int grid_x, int grid_y, int width, lgl_color color, lgl_text * text_engine)
	: lgl_indicator(grid_x, grid_y, width, color, text_engine){}

void lgl_edit_indicator::on_left_mouse_press(){
	m_capture_input = true;

	m_text = "";
}

void lgl_edit_indicator::on_keyboard(int key, int scancode, int action, int mods){
	if(action == lgl_window::KEY_ACTION_PRESS){
		if(key == lgl_window::KEY_BACKSPACE){
			if(m_text.size() > 0){
				m_text = m_text.erase(m_text.length() - 1, 1);
			}
		}
		else if(key == lgl_window::KEY_ENTER || key == lgl_window::KEY_ENTER_2){
			m_capture_input = false;

			on_edit_complete();
		}
		else if(key == lgl_window::KEY_ESCAPE){
			m_capture_input = false;

			on_edit_cancel();
		}
	}
}

void lgl_edit_indicator::on_text_input(unsigned int key){
	if(key < 128){
		if(m_text[0] == ' '){
			m_text.erase(0, 1);
		}

		m_text += (char) key;
	}
}

void lgl_edit_indicator::on_edit_complete(){}
void lgl_edit_indicator::on_edit_cancel(){}
