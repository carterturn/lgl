#include "lgl_primatives.h"

#include <cmath>

#include <GL/gl.h>

#include <iostream>
using std::cout;

void lgl_set_color(float R, float G, float B){
	lgl_set_color(R, G, B, 1.0f);
}

void lgl_set_color(float R, float G, float B, float alpha){
	glColor4f(R, G, B, alpha);
}

void lgl_draw_rectangle(float x, float y, float width, float height){
	glBegin(GL_QUADS);

	glVertex2f(x, y);
	glVertex2f(x + width, y);
	glVertex2f(x + width, y + height);
	glVertex2f(x, y + height);

	glEnd();
}

void lgl_arc_inside(float center_x, float center_y, bool forwards, bool upwards){
	glBegin(GL_POLYGON);

	glVertex2f(center_x + (forwards ? 0.5f : -0.5f), center_y);

	float flip = upwards ? 1.0f : -1.0f;

	for(float i = 0; i < 0.5f; i+=0.02f){
		glVertex2f(center_x + (forwards ? i : -i), center_y + flip * sqrt(1.0f - (i*2.0f-1.0f) * (i*2.0f-1.0f)));
	}

	glVertex2f(center_x + (forwards ? 1 : -1), center_y + (upwards ? 1 : 0));
	glVertex2f(center_x + (forwards ? 1 : -1), center_y + (upwards ? 0 : -1));

	glVertex2f(center_x + (forwards ? 0.5f : -0.5f), upwards ? 1 : -1);

	glEnd();
}

void lgl_arc_outside(float center_x, float center_y, bool forwards, bool upwards){
	glBegin(GL_POLYGON);

	float flip = upwards ? 1.0f : -1.0f;

	glVertex2f(center_x, center_y + flip);

	for(float i = 0; i < 0.5f; i+=0.02f){
		glVertex2f(center_x + (forwards ? i : -i),
				   center_y + flip * sqrt(1.0f - (i - 1.0f) * (i - 1.0f)));
	}

	glEnd();
}
