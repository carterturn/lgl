#include "lgl_text.h"

#include <iostream>
#include <fstream>

#ifdef WINDOWS
#include <GL/glext.h>
#endif

using std::cerr;
using std::cout;
using std::fstream;

uint32_t make_uint32(uint8_t * bytes){
	return (bytes[3] << 24) + (bytes[2] << 16) + (bytes[1] << 8) + bytes[0];
}

lgl_text::lgl_text(string font_bitmap) : m_font_bitmap_filename(font_bitmap) {}

int lgl_text::initialize(){
	fstream font_bitmap(m_font_bitmap_filename);

	if(!font_bitmap.is_open()){
		cerr << "Font file not found\n";
		return -1; // File not found
	}
	
	unsigned char * header = new unsigned char[14];
	font_bitmap.read((char *) header, 14);

	if(header[0] != 'B' || header[1] != 'M'){
		cerr << "Font file invalid\n";
		return -2; // Invalid font file
	}

	uint32_t file_size = make_uint32(header + 2);
	uint32_t pixel_offset = make_uint32(header + 10);

	delete[] header;

	unsigned char * DIB_size = new unsigned char[4];
	font_bitmap.read((char *) DIB_size, 4);

	uint32_t dib_header_size = make_uint32(DIB_size);

	delete[] DIB_size;
	
	if(dib_header_size != 40 && dib_header_size != 108 && dib_header_size != 124){
		// We only accept BITMAPINFOHEADER, BITMAPV4HEADER, and BITMAPV5 header
		cerr << "Font file invalid header\n";
		return -2;
	}

	unsigned char * dib_header = new unsigned char[40]; // Only use the BITMAPINFOHEADER
	font_bitmap.read((char *) dib_header, 40);

	uint32_t width = make_uint32(dib_header);
	uint32_t height = make_uint32(dib_header + 4);
	uint16_t bits_per_pixel = (dib_header[11] << 8) + dib_header[10];

	if(bits_per_pixel != 8){
		cerr << "Font file invalid data format\n";
		return -2;
	}

	delete[] dib_header;

	uint8_t * pixel_data = new uint8_t[width * height];

	font_bitmap.seekg(pixel_offset + 1);
	font_bitmap.read((char *) pixel_data, width * height);

	font_bitmap.close();

	glGenTextures(1, &m_font_texture);
	glBindTexture(GL_TEXTURE_2D, m_font_texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
	glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
	glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RED,  GL_UNSIGNED_BYTE, pixel_data);

	return 0;
}

void lgl_text::draw_text(string text, float x_start, float y_start, float total_length, float R, float G, float B){
	draw_text(text, x_start, y_start, total_length, total_length, R, G, B);
}

void lgl_text::draw_text(string text, float x_start, float y_start, float height, float length,
						 float R, float G, float B){

	float char_width, char_height;
	if(length / (float) text.length() < height / 4.0f){
		char_width = length / text.length();
		char_height = char_width * 4.0f;
	}
	else{
		char_height = height;
		char_width = char_height / 4.0f;
	}

	float x_position = x_start;
	float y_position = y_start;

	glBindTexture(GL_TEXTURE_2D, m_font_texture);

	float text_color[4] = {R, G, B, 0.0f};
	glTexEnvfv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, text_color);

	GLint swizzle_mask[4] = {GL_RED, GL_RED, GL_RED, GL_RED};
	glTexParameteriv(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_RGBA, swizzle_mask);
	
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE);

	glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_MODULATE);
	glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE0_RGB, GL_CONSTANT);
	glTexEnvi(GL_TEXTURE_ENV, GL_OPERAND0_RGB, GL_SRC_COLOR);
	glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE1_RGB, GL_TEXTURE);
	glTexEnvi(GL_TEXTURE_ENV, GL_OPERAND1_RGB, GL_SRC_COLOR);

	glAlphaFunc(GL_GREATER, 0.5f);

	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_ALPHA_TEST);
	glEnable(GL_TEXTURE_2D);

	glBegin(GL_QUADS);

	for(int i = 0; i < text.size(); i++){
		if(text[i] == ' '){
			x_position += char_width;
			continue;
		}

		glTexCoord2f((1.0/94.0) * (text[i] - 33), 0.0f); glVertex3f(x_position, y_position, 0.0f);
		glTexCoord2f((1.0/94.0) * (text[i] - 33), 1.0f); glVertex3f(x_position, y_position + char_height, 0.0f);
		glTexCoord2f((1.0/94.0) * (text[i] - 32), 1.0f); glVertex3f(x_position + char_width,
																	y_position + char_height, 0.0f);
		glTexCoord2f((1.0/94.0) * (text[i] - 32), 0.0f); glVertex3f(x_position + char_width, y_position, 0.0f);

		x_position += char_width;
	}

	glEnd();

	glDisable(GL_TEXTURE_2D);
}
