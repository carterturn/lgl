/*
  Copyright 2018 Carter Turnbaugh

  This file is part of LGL.

  LGL is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  LGL is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with LGL.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lgl_indicator.h"
#include "lgl_primatives.h"

lgl_indicator::lgl_indicator(int grid_x, int grid_y, int width, lgl_color color, lgl_text * text_engine)
	: lgl_object(text_engine), m_width(width), m_color(color) {
	m_grid_x_min = grid_x;
	m_grid_x_max = grid_x + width;
	m_grid_y_min = grid_y;
	m_grid_y_max = grid_y + 1;

	m_scale_draw = false;

	m_text = "";
}

void lgl_indicator::set_text(string text){
	m_text = text;
}

string lgl_indicator::get_text(){
	return m_text;
}

void lgl_indicator::draw(){
	lgl_set_color(m_color.R, m_color.G, m_color.B);
	
	lgl_draw_rectangle(0.0078125, 0.015625, 0.125 - 0.0078125, 1 - 2.0f*0.015625);

	m_text_engine->draw_text(m_text, 0.125, 0.0625, 1-2.0f*0.015625, m_width-0.375, m_color.R, m_color.G, m_color.B);

	lgl_draw_rectangle(m_width - 0.25, 0.015625, 0.25 - 0.0078125, 1 - 2.0f*0.015625);
}

void lgl_indicator::on_hover_start(float x, float y){
	// All hovering within range
	on_hover_start();
}

void lgl_indicator::on_left_mouse_press(float x, float y){
	on_left_mouse_press();
}

void lgl_indicator::on_left_mouse_release(float x, float y){
	on_left_mouse_release();
}

void lgl_indicator::on_right_mouse_press(float x, float y){
	on_right_mouse_press();
}

void lgl_indicator::on_right_mouse_release(float x, float y){
	on_right_mouse_release();
}


void lgl_indicator::on_hover_start(){}
void lgl_indicator::on_hover_end(){}
void lgl_indicator::on_left_mouse_press(){}
void lgl_indicator::on_left_mouse_release(){}
void lgl_indicator::on_right_mouse_press(){}
void lgl_indicator::on_right_mouse_release(){}

void lgl_indicator::set_color(lgl_color color){
	m_color = color;
}

void lgl_indicator::set_color(float R, float G, float B){
	set_color({R, G, B});
}
