/*
  Copyright 2018 Carter Turnbaugh

  This file is part of LGL.

  LGL is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  LGL is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with LGL.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lgl_button.h"
#include "lgl_primatives.h"

lgl_button::lgl_button(int grid_x, int grid_y, int height, string text, lgl_color color, lgl_text * text_engine)
	: lgl_object(text_engine), m_height(height), m_text(text), m_color(color) {
	m_grid_x_min = grid_x;
	m_grid_x_max = grid_x + 1;
	m_grid_y_min = grid_y;
	m_grid_y_max = grid_y + height;

	m_scale_draw = false;
}

void lgl_button::draw(){
	lgl_set_color(m_color.R, m_color.G, m_color.B);
	
	lgl_draw_rectangle(0.0078125, 0.015625, 1 - 0.015625, m_height - 0.03125);

	m_text_engine->draw_text(m_text, 0.0625, 0.0625, m_height-0.03125, 0.875, 0.0f, 0.0f, 0.0f);
}

void lgl_button::on_hover_start(float x, float y){
	// All hovering within range
	on_hover_start();
}

void lgl_button::on_left_mouse_press(float x, float y){
	on_left_mouse_press();
}

void lgl_button::on_left_mouse_release(float x, float y){
	on_left_mouse_release();
}

void lgl_button::on_right_mouse_press(float x, float y){
	on_right_mouse_press();
}

void lgl_button::on_right_mouse_release(float x, float y){
	on_right_mouse_release();
}


void lgl_button::on_hover_start(){}
void lgl_button::on_hover_end(){}
void lgl_button::on_left_mouse_press(){}
void lgl_button::on_left_mouse_release(){}
void lgl_button::on_right_mouse_press(){}
void lgl_button::on_right_mouse_release(){}

void lgl_button::set_color(lgl_color color){
	m_color = color;
}

void lgl_button::set_color(float R, float G, float B){
	set_color({R, G, B});
}
