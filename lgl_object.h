/*
  Copyright 2018 Carter Turnbaugh

  This file is part of LGL.

  LGL is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  LGL is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with LGL.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "lgl_text.h"

#include <string>

using std::string;

struct lgl_color {float R; float G; float B;};

class lgl_object {
public:
	lgl_object(lgl_text * text_engine);

	float get_grid_x_min();
	float get_grid_x_max();
	float get_grid_y_min();
	float get_grid_y_max();
	bool get_scale_draw();

	virtual void draw();

	virtual void on_hover_start(float x, float y);
	virtual void on_hover_end();
	virtual void on_left_mouse_press(float x, float y);
	virtual void on_left_mouse_release(float x, float y);
	virtual void on_right_mouse_press(float x, float y);
	virtual void on_right_mouse_release(float x, float y);

	bool get_capture_input();
	virtual void on_keyboard(int key, int scancode, int action, int mods);
	virtual void on_text_input(unsigned int key);

	virtual void on_scroll(double x_offset, double y_offset);

protected:

	float m_grid_x_min, m_grid_x_max, m_grid_y_min, m_grid_y_max;
	bool m_scale_draw;

	bool m_capture_input;

	lgl_text * m_text_engine;
};
