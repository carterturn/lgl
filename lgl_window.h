/*
  Copyright 2018 Carter Turnbaugh

  This file is part of LGL.

  LGL is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  LGL is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with LGL.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <GLFW/glfw3.h>

#include <list>
#include <string>

#include "lgl_object.h"

using std::list;
using std::string;

class lgl_window {
public:
	lgl_window(string title, int grid_x = 1, int grid_y = 1);

	int initialize();
	void cleanup();

	void resize();
	void resize(int grid_x, int grid_y);

	void add_object(lgl_object * new_object);
	void clear_objects();

	void update();

	bool keep_running();

	constexpr static unsigned int KEY_ACTION_PRESS = GLFW_PRESS;
	constexpr static unsigned int KEY_ACTION_RELEASE = GLFW_RELEASE;

	constexpr static unsigned int KEY_BACKSPACE = GLFW_KEY_BACKSPACE;
	constexpr static unsigned int KEY_ENTER = GLFW_KEY_ENTER;
	constexpr static unsigned int KEY_ENTER_2 = GLFW_KEY_KP_ENTER;
	constexpr static unsigned int KEY_ESCAPE = GLFW_KEY_ESCAPE;
	constexpr static unsigned int KEY_RIGHT = GLFW_KEY_RIGHT;
	constexpr static unsigned int KEY_LEFT = GLFW_KEY_LEFT;
	constexpr static unsigned int KEY_UP = GLFW_KEY_UP;
	constexpr static unsigned int KEY_DOWN = GLFW_KEY_DOWN;

protected:

	void on_click(float x, float y, int button, int action, int modifiers);
	void cursor_position(float x, float y);
	void cursor_enter(int entered);
	void keyboard(int key, int scancode, int action, int mods);
	void text_input(unsigned int key);
	void scroll(double x_offset, double y_offset);

	string m_title;
	int m_grid_x, m_grid_y;

	constexpr static unsigned int GRID_WIDTH = 120;
	constexpr static unsigned int GRID_HEIGHT = 48;
	constexpr static unsigned int GRID_BORDER = 2;

	constexpr static unsigned int GRID_WIDTH_FULL = GRID_WIDTH + GRID_BORDER * 2;
	constexpr static unsigned int GRID_HEIGHT_FULL = GRID_HEIGHT + GRID_BORDER * 2;

	list<lgl_object *> m_objects;

private:

	static void on_click_bootstrap(GLFWwindow * glfw_window, int button, int action, int modifiers);
	static void cursor_position_bootstrap(GLFWwindow * glfw_window, double x, double y);
	static void cursor_enter_bootstrap(GLFWwindow * glfw_window, int entered);
	static void keyboard_bootstrap(GLFWwindow * glfw_window, int key, int scancode, int action, int mods);
	static void text_input_bootstrap(GLFWwindow * glfw_window, unsigned int key);
	static void scroll_bootstrap(GLFWwindow * glfw_window, double x_offset, double y_offset);

	GLFWwindow * m_glfw_window;

	bool m_keep_running;
};
