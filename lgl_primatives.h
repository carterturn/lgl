/*
  Copyright 2018 Carter Turnbaugh

  This file is part of LGL.

  LGL is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  LGL is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with LGL.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

void lgl_set_color(float R, float G, float B);
void lgl_set_color(float R, float G, float B, float alpha);

void lgl_draw_rectangle(float x, float y, float width, float height);

void lgl_arc_inside(float center_x, float center_y, bool forwards, bool upwards);

void lgl_arc_outside(float center_x, float center_y, bool forwards, bool upwards);
