#pragma once

#include "lgl_object.h"

constexpr lgl_color ERA_4_1 = {0.5f, 0.5f, 0.5f};
constexpr lgl_color ERA_4_2 = {0.8f, 0.867f, 1.0f};
constexpr lgl_color ERA_4_3 = {0.333f, 0.6f, 1.0f};
constexpr lgl_color ERA_4_4 = {0.2f, 0.4f, 1.0f};
constexpr lgl_color ERA_4_5 = {0.0f, 0.067f, 0.933f};
constexpr lgl_color ERA_4_6 = {0.0f, 0.0f, 0.533f};
constexpr lgl_color ERA_4_7 = {0.733f, 0.667f, 0.333f};
constexpr lgl_color ERA_4_8 = {0.733f, 0.267f, 0.067f};
constexpr lgl_color ERA_4_9 = {0.533f, 0.133f, 0.067f};
