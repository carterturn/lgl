/*
  Copyright 2018 Carter Turnbaugh

  This file is part of LGL.

  LGL is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  LGL is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with LGL.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "lgl_object.h"
#include "lgl_text.h"

#include <vector>

using std::string;
using std::vector;

class lgl_text_display : public lgl_object {
public:
	lgl_text_display(int grid_x, int grid_y, int height, int width, lgl_color color, lgl_text * text_engine);

	void set_text_data(vector<string> data);

	void draw() override;

	void on_hover_start(float x, float y) override;
	void on_hover_end() override;

	void on_left_mouse_press(float x, float y) override;

	virtual void on_left_mouse_press(string item);

	void on_scroll(double x_offset, double y_offset) override;

protected:
	int m_height;
	int m_width;

	lgl_color m_color;

	int m_data_start;
	vector<string> m_data;
	float m_hover_y;
};
