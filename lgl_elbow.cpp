/*
  Copyright 2018 Carter Turnbaugh

  This file is part of LGL.

  LGL is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  LGL is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with LGL.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lgl_elbow.h"
#include "lgl_primatives.h"

lgl_elbow::lgl_elbow(int grid_x, int grid_y, int height, int length, int orientation,
					 string text, lgl_color color, lgl_text * text_engine)
	: lgl_object(text_engine), m_height(height), m_length(length), m_orientation(orientation),
	  m_text(text), m_color(color) {
	m_grid_x_min = grid_x;
	m_grid_x_max = grid_x + length + 2;
	m_grid_y_min = grid_y;
	m_grid_y_max = grid_y + height + 2;

	m_scale_draw = false;
}

void lgl_elbow::draw(){
	lgl_set_color(m_color.R, m_color.G, m_color.B);

	float border_x = 0.0078125f;
	float border_y = 0.015625f;

	if(m_orientation == ORIENT_BOTTOM_LEFT){
		lgl_arc_inside(0, 1, true, false);
		lgl_draw_rectangle(1, border_y, m_length + 1 - border_x, 0.33 - border_y);
		lgl_arc_outside(1, 1, true, false);
		lgl_draw_rectangle(border_x, 1, 1 - 2.0f*border_x, m_height + 1 - border_y);

		m_text_engine->draw_text(m_text, 0.0625, m_height + 1 + 0.0625, 1.0, 0.875, 0.0f, 0.0f, 0.0f);

		lgl_set_color(0.0f, 0.0f, 0.0f);

		lgl_draw_rectangle(0, 0, 2, border_y);
		lgl_draw_rectangle(0, 0, border_x, 2);
 	}
	else if(m_orientation == ORIENT_TOP_LEFT){
		lgl_arc_inside(0, m_height + 1, true, true);
		lgl_draw_rectangle(1, m_height + 1.66, m_length + 1 - border_x, 0.33 - border_y);
		lgl_arc_outside(1, m_height + 1, true, true);
		lgl_draw_rectangle(border_x, border_y, 1 - 2.0f*border_x, m_height + 1 - border_y);

		m_text_engine->draw_text(m_text, 0.0625, 0.0625, 1.0, 0.875, 0.0f, 0.0f, 0.0f);

		lgl_set_color(0.0f, 0.0f, 0.0f);

		lgl_draw_rectangle(0, m_height + 2 - border_y, 2, border_y);
		lgl_draw_rectangle(0, m_height - 1, border_x, 2);
	}
	else if(m_orientation == ORIENT_BOTTOM_RIGHT){
		lgl_arc_inside(m_length + 2, 1, false, false);
		lgl_draw_rectangle(border_x, border_y, m_length + 1 - border_x, 0.33 - border_y);
		lgl_arc_outside(m_length + 1, 1, false, false);
		lgl_draw_rectangle(m_length + 1 + border_x, 1, 1 - 2.0f*border_x, m_height + 1 - border_y);

		m_text_engine->draw_text(m_text, m_length + 1 + 0.0625, m_height + 1 + 0.0625, 1.0, 0.875, 0.0f, 0.0f, 0.0f);

		lgl_set_color(0.0f, 0.0f, 0.0f);

		lgl_draw_rectangle(m_length, 0, 2, border_y);
		lgl_draw_rectangle(m_length + 2 - border_x, 0, border_x, 2);
	}
	else if(m_orientation == ORIENT_TOP_RIGHT){
		lgl_arc_inside(m_length + 2, m_height + 1, false, true);
		lgl_draw_rectangle(border_x, m_height + 1.66, m_length + 1 - border_x, 0.33 - border_y);
		lgl_arc_outside(m_length + 1, m_height + 1, false, true);
		lgl_draw_rectangle(m_length + 1 + border_x, border_y, 1 - 2.0f*border_x, m_height + 1 - border_y);

		m_text_engine->draw_text(m_text, m_length + 1 + 0.0625, 0.0625, 1.0, 0.875, 0.0f, 0.0f, 0.0f);

		lgl_set_color(0.0f, 0.0f, 0.0f);

		lgl_draw_rectangle(m_length, m_height + 2 - border_y, 2, border_y);
		lgl_draw_rectangle(m_length + 2 - border_x, m_height, border_x, 2);
	}
}

void lgl_elbow::on_hover_start(float x, float y){
	if(m_orientation == ORIENT_BOTTOM_LEFT || m_orientation == ORIENT_TOP_LEFT){
		if(x < 1){
			on_hover_start();
		}
	}
	else{
		if(x > m_length + 1){
			on_hover_start();
		}
	}
}

void lgl_elbow::on_left_mouse_press(float x, float y){
	if(m_orientation == ORIENT_BOTTOM_LEFT || m_orientation == ORIENT_TOP_LEFT){
		if(x < 1){
			on_left_mouse_press();
		}
	}
	else{
		if(x > m_length + 1){
			on_left_mouse_press();
		}
	}
}

void lgl_elbow::on_left_mouse_release(float x, float y){
	if(m_orientation == ORIENT_BOTTOM_LEFT || m_orientation == ORIENT_TOP_LEFT){
		if(x < 1){
			on_left_mouse_release();
		}
	}
	else{
		if(x > m_length + 1){
			on_left_mouse_release();
		}
	}
}

void lgl_elbow::on_right_mouse_press(float x, float y){
	if(m_orientation == ORIENT_BOTTOM_LEFT || m_orientation == ORIENT_TOP_LEFT){
		if(x < 1){
			on_right_mouse_press();
		}
	}
	else{
		if(x > m_length + 1){
			on_right_mouse_press();
		}
	}
}

void lgl_elbow::on_right_mouse_release(float x, float y){
	if(m_orientation == ORIENT_BOTTOM_LEFT || m_orientation == ORIENT_TOP_LEFT){
		if(x < 1){
			on_right_mouse_release();
		}
	}
	else{
		if(x > m_length + 1){
			on_right_mouse_release();
		}
	}
}


void lgl_elbow::on_hover_start(){}
void lgl_elbow::on_hover_end(){}
void lgl_elbow::on_left_mouse_press(){}
void lgl_elbow::on_left_mouse_release(){}
void lgl_elbow::on_right_mouse_press(){}
void lgl_elbow::on_right_mouse_release(){}

void lgl_elbow::set_color(lgl_color color){
	m_color = color;
}

void lgl_elbow::set_color(float R, float G, float B){
	set_color({R, G, B});
}
