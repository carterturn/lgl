/*
  Copyright 2018 Carter Turnbaugh

  This file is part of LGL.

  LGL is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  LGL is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with LGL.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "lgl_object.h"
#include "lgl_text.h"

using std::string;

class lgl_elbow : public lgl_object {
public:
	constexpr static unsigned int ORIENT_BOTTOM_LEFT = 0;
	constexpr static unsigned int ORIENT_TOP_LEFT = 1;
	constexpr static unsigned int ORIENT_BOTTOM_RIGHT = 2;
	constexpr static unsigned int ORIENT_TOP_RIGHT = 3;

	lgl_elbow(int grid_x, int grid_y, int height, int length, int orientation,
			  string text, lgl_color color, lgl_text * text_engine);

	void draw();

	void on_hover_start(float x, float y);
	void on_left_mouse_press(float x, float y);
	void on_left_mouse_release(float x, float y);
	void on_right_mouse_press(float x, float y);
	void on_right_mouse_release(float x, float y);

	virtual void on_hover_start();
	virtual void on_hover_end();
	virtual void on_left_mouse_press();
	virtual void on_left_mouse_release();
	virtual void on_right_mouse_press();
	virtual void on_right_mouse_release();

protected:
	int m_height, m_length, m_orientation;

	string m_text;

	void set_color(lgl_color color);
	void set_color(float R, float G, float B);

	lgl_color m_color;
};
